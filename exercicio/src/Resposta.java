import java.io.*;
import java.io.IOException;

public class Resposta {
    private static final String PATH_ASCII = "/Users/cr/dev/testeASCII.txt";
    private static final String PATH_BINARIO = "/Users/cr/dev/testeBinario.bin";

    public static void main(String[] args) {
        System.out.println("Arquivo ASCII:");
        try {
            BufferedReader br = new BufferedReader(new FileReader(PATH_ASCII));
            while (br.ready()) {
                System.out.println(br.readLine());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Arquivo BINARIO:");
        File file = new File(PATH_BINARIO);
        try (FileInputStream fileInputStream = new FileInputStream(file)) {
            int singleCharInt;

            while ((singleCharInt = fileInputStream.read()) != -1) {
                System.out.print(singleCharInt);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}