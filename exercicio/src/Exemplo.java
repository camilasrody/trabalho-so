import java.io.IOException;
import java.io.PrintWriter;

public class Exemplo {

    private static final String PATH_ASCII = "/Users/cr/dev/testeASCII.txt";
    private static final String PATH_BINARIO = "/Users/cr/dev/testeBinario.bin";
    private static final int N = 1000;

    public static void main(String[] args) {
        // escrevendo de 0 a N no formato ASCII
        try (PrintWriter writer = new PrintWriter(PATH_ASCII, "US-ASCII")) {
            for (int i = 0; i < N; i++)
                writer.print(i);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // escrevendo de 0 a N em binário
        try (PrintWriter writer = new PrintWriter(PATH_BINARIO)) {
            for (int i = 0; i < N; i++)
                writer.write(i);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Fim!");
    }
}

